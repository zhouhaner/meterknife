﻿namespace MeterKnife.Keysights.VISAs
{
    public enum GPIBLogLevel
    {
        Trace = 0,
        Warn = 1,
        Error = 2
    }
}