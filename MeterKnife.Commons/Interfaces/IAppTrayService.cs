﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NKnife.Interface;

namespace MeterKnife.Interfaces
{
    /// <summary>
    /// 描述本应用程序实现的程序托盘的功能接口
    /// </summary>
    public interface IAppTrayService : IEnvironmentItem
    {
    }
}
