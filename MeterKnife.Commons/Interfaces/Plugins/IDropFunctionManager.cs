﻿using System.Collections.Generic;
using System.Windows.Forms;
using MeterKnife.Base.Plugins;

namespace MeterKnife.Interfaces.Plugins
{
    public interface IDropFunctionManager : IDictionary<PluginStyle, PluginViewComponent>
    {
    }
}
