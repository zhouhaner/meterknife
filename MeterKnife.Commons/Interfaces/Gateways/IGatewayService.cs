using System.Collections.Generic;
using MeterKnife.Base.Channels;
using NKnife.Interface;

namespace MeterKnife.Interfaces.Gateways
{
    /// <summary>
    /// 描述一个全局的测量途径服务
    /// </summary>
    public interface IGatewayService : IEnvironmentItem
    {
    }
}