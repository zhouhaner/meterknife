﻿namespace MeterKnife.Interfaces.Gateways
{
    /// <summary>
    /// 测量途径的模式
    /// </summary>
    public enum GatewayModel
    {
        CareOne,
        CareTwo,
        Aglient82357A,
        Aglient82357B,
        SerialPort,
        TcpIp,
        USB
    }
}
