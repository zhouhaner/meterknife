namespace MeterKnife.Base.Plugins
{
    /// <summary>
    /// 插件类型
    /// </summary>
    public enum PluginStyle
    {
        /// <summary>
        /// 文件菜单的功能
        /// </summary>
        FileMenu,
        /// <summary>
        /// 测量菜单的功能
        /// </summary>
        MeasureMenu,
        /// <summary>
        /// 数据菜单的功能
        /// </summary>
        DataMenu,
        /// <summary>
        /// 工具菜单的功能
        /// </summary>
        ToolMenu,
        /// <summary>
        /// 视图菜单的功能
        /// </summary>
        ViewMenu,
        /// <summary>
        /// 帮助菜单的功能
        /// </summary>
        HelpMenu
    }

}