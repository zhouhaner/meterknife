## 概述
- 本软件是开源软件。
- 本软件可广泛应用于电子行业的测试与测量仪器的计算机控制，软件能够进行高性能的检查与测试。可用于万用表，电源，频率计，示波器等仪器，您可以很容易地利用它建立自动检查与测试系统。这能够使您将传统的目视检查处理过程转换到基于计算机的自动化测试。可让您的仪器发挥最大功效，进行深入的设计分析，并可节省测试时间。
- 本软件致力于在多种PC连接环境下提供快速方便的不同品牌仪器连接、命令控制、数据采集与分析。
- 本软件的初始发源来自开源的设备MeterCare。这是一个“GPIB->串口与LAN控制器”，该硬件提供了一种经由标准串口与网络(有线或无线)来远程访问和操作具备GPIB接口与支持SCPI协议的仪器的手段。使用该设备，用户可以充分利用现有串口或网络轻松实现测试设备和数据的共享。借助于该网关，用户能够在一个更安全、更方便的位置使用计算机进行操作，而不必依赖带有GPIB接口卡的本地计算机。该硬件的第一代设计已完成。该开源硬件项目代码：[https://git.oschina.net/gpib/CARE](https://git.oschina.net/gpib/CARE)

### 关于
作者的Blog: [http://xknife.net](http://xknife.net)
