﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("MeterKnife自动化测量测试平台")]
[assembly: AssemblyProduct("MeterKnife")]
[assembly: AssemblyDescription("　　本软件可广泛应用于电子行业的测试与测量仪器的计算机控制，软件能够进行高性能的检查与测试。可用于万用表，电源，频率计，示波器等仪器，您可以很容易地利用它建立自动检查与测试系统。这能够使您将传统的目视检查处理过程转换到基于计算机的自动化测试。可让您的仪器发挥最大功效，进行深入的设计分析，并可节省测试时间。\r\n　　本软件致力于在多种PC连接环境下提供快速方便的不同品牌仪器连接、命令控制、数据采集与分析。")]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("974f3368-91fd-405b-88de-3cdd84dc23c5")]

