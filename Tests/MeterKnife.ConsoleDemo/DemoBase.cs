﻿namespace MeterKnife.ConsoleDemo
{
    public abstract class DemoBase
    {
        public abstract void Run();
    }
}