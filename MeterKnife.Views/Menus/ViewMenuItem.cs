﻿using System.Windows.Forms;
using MeterKnife.Base;

namespace MeterKnife.Views.Menus
{
    public sealed class ViewMenuItem : OrderToolStripMenuItem
    {
        public ViewMenuItem()
        {
            Text = "视图(&V)";
        }
    }
}