﻿using System.Windows.Forms;
using MeterKnife.Base;

namespace MeterKnife.Views.Menus
{
    public sealed class DataMenuItem : OrderToolStripMenuItem
    {
        public DataMenuItem()
        {
            Text = "数据(&D)";
        }
    }
}
