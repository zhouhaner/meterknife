﻿using System.Windows.Forms;
using MeterKnife.Base;

namespace MeterKnife.Views.Menus
{
    public sealed class MeasureMenuItem : OrderToolStripMenuItem
    {
        public MeasureMenuItem()
        {
            Text = "测量(&M)";
        }
    }
}
