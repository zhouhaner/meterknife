﻿using System.Windows.Forms;
using MeterKnife.Base;

namespace MeterKnife.Views.Menus
{
    public sealed class ToolMenuItem : OrderToolStripMenuItem
    {
        public ToolMenuItem()
        {
            Text = "工具(&T)";
        }
    }
}
