﻿using System.Collections;
using System.Windows.Forms;
using MeterKnife.Base;
using NKnife.IoC;

namespace MeterKnife.Views.Menus
{
    public sealed class FileMenuItem : OrderToolStripMenuItem
    {
        public FileMenuItem()
        {
            Text = "文件(&F)";
        }
    }
}
