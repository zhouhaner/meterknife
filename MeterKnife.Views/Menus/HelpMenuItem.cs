﻿using System.Windows.Forms;
using MeterKnife.Base;

namespace MeterKnife.Views.Menus
{
    public sealed class HelpMenuItem : OrderToolStripMenuItem
    {
        public HelpMenuItem()
        {
            Text = "帮助(&H)";
        }
    }
}